/*  Autor: Fabio Duarte de Souza
    Disciplina: Estrutura de dados II
    Professora: Priscyla Waleska T de A Simoes
    Referente: Exerc�cio 14
    Finalidade: Representar o funcionamento de uma fila de banco utilizando STL
    
 */


#include "funcoes.h"

int menu(int *op);

main()
{
    int op;
    string cliente;
    queue<string> *f = new queue<string>;
    
    do
    {
        menu(&op);
        switch(op)
        {
            case 1: cout<<"\nDigite o nome da pessoa que entrara na fila: ";
                    cin>>cliente;
                    f->push(cliente);
                    cout<<"\n"<<cliente<<" entrou na fila.\n";
                    system("pause");
                    break;

            case 2: if (!f->empty())
                    {
                        cout<<"\n"<<f->front()<<" foi atendido(a) e saiu da fila.\n";
                        f->pop();
                    }
                    else
                        cout<<"\nA fila esta vazia\n";
                    system("pause");
                    break;

            case 3: if (!f->empty())
                    {
                        cout<<"\nProximo a ser atendido(a): "<<f->front();
                        cout<<"\n";
                    }
                    else
                        cout<<"\nA fila esta vazia\n";
                    system("pause");
                    break;
            
            case 4: break;
            
            default: cout<<"\nOpcao invalida\n";
                     system("pause");
                     
        }
    } while(op != 4);
}

int menu(int *op)
{
    system("cls");
    cout<<"   Simulador de uma fila de banco\n\n";
    cout<<"\t1 - Entrar na fila\n";
    cout<<"\t2 - Sair na fila\n";
    cout<<"\t3 - Mostrar quem sera atendido\n";
    cout<<"\t4 - Sair\n";
    cout<<"\n   Digite uma opcao: ";
    cin>>*op;
}
